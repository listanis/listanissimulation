package Controllers;

import Map.Time;

/**
 * This class is responsible for controlling the behavior of animals in the simulation.
 * It interacts with the MapController to update the state of the map and the Time object to keep track of the simulation time.
 */
public class AnimalController {
    /**
     * The Time object that keeps track of the simulation time.
     */
    private Time time;

    /**
     * The MapController object that is responsible for updating the state of the map.
     */
    private MapController mapController;

    /**
     * Constructor for the AnimalController class. Initializes a new AnimalController with the specified Time and MapController objects.
     *
     * @param time The Time object that keeps track of the simulation time.
     * @param mapController The MapController object that is responsible for updating the state of the map.
     */
    public AnimalController(Time time, MapController mapController) {
        this.time = time;
        this.mapController = mapController;
    }

    /**
     * Calls the updateCurrentChunk method of the MapController object to update the current chunk of the map.
     */
    public void updateCurrentChunk() {
        mapController.updateCurrentChunk();
    }
}
