package Map;

/**
 * Represents the simulation time.
 */
public class Time {
    public int days = 0;
    public int hours = 0;
    public int minutes = 0;
    public int seconds = 0;

    public String timeMode;

    /**
     * Constructs a Time object with the specified time mode.
     *
     * @param timeMode The time mode ("real_time" or "skip_day").
     */
    public Time(String timeMode) {
        this.timeMode = timeMode;
    }

    /**
     * Increases the simulation time by one second.
     */
    public void tick() {
        seconds += 1;
        if (seconds >= 60) {
            seconds = 0;
            minutes += 1;
            if (minutes >= 60) {
                minutes = 0;
                hours += 1;
                if (hours >= 24) {
                    hours = 0;
                    days += 1;
                }
            }
        }
    }

    /**
     * Increases the simulation time by one day.
     */
    public void tickDay() {
        days += 1;
    }

    /**
     * Resets the time to start of day.
     */
    public void resetTime() {
        hours = 0;
        minutes = 0;
    }

    /**
     * Returns the string representation of the time.
     *
     * @return The string representation of the time.
     */
    @Override
    public String toString() {
        return String.format("TIME |%02d:%02d:%02d:%02d|", days, hours, minutes, seconds);
    }

    /**
     * Sets the time mode.
     *
     * @param realTime The time mode to set ("real_time" or "skip_day").
     */
    public void setMode(String realTime) {
        timeMode = realTime;
    }

    /**
     * Gets the current time mode.
     *
     * @return The current time mode.
     */
    public String getMode() {
        return timeMode;
    }
}
